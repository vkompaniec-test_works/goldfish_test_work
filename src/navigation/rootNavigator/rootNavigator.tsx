import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { NavigationContainer } from '@react-navigation/native';
import { ScreenRoutes } from "../screenRoutes";
import { AuthNavigator } from "../authNavigator/authNavigator";
import { stackOptions } from "../options";

const Stack = createNativeStackNavigator();


export const RootNavigator = () =>
    <NavigationContainer>
        <Stack.Navigator screenOptions={stackOptions}>
            <Stack.Screen name={ScreenRoutes.AUTH_STACK} component={AuthNavigator} />
        </Stack.Navigator>
    </NavigationContainer>