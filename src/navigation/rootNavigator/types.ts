import { ScreenRoutes } from "../screenRoutes";

export type RootNavigatorParamList = {
    [ScreenRoutes.AUTH_STACK]: undefined;
}