import { ScreenRoutes } from "../screenRoutes"
import type { NativeStackScreenProps } from '@react-navigation/native-stack';

export type AuthStackParamList = {
    [ScreenRoutes.INIT_SCREEN]: undefined;
    [ScreenRoutes.REGISTRATION_SCREEN]: undefined;
    [ScreenRoutes.OTP_SCREEN]: undefined
}

export type InitScreenProps = NativeStackScreenProps<AuthStackParamList, ScreenRoutes.INIT_SCREEN>