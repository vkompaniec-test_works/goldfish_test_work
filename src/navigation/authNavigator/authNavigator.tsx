import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { InitScreen } from "../../features/auth/screens/InitScreen";
import { OtpScreen } from "../../features/auth/screens/OtpScreen";
import { RegistrationScreen } from "../../features/auth/screens/RegistrationScreen";
import { ScreenRoutes } from "../screenRoutes";
import { AuthStackParamList } from "./types";
import { stackOptions } from "../options";

const Stack = createNativeStackNavigator<AuthStackParamList>();

export const AuthNavigator = () =>
    <Stack.Navigator screenOptions={stackOptions}>
        <Stack.Screen name={ScreenRoutes.INIT_SCREEN} component={InitScreen} />
        <Stack.Screen name={ScreenRoutes.REGISTRATION_SCREEN} component={RegistrationScreen} />
        <Stack.Screen name={ScreenRoutes.OTP_SCREEN} component={OtpScreen} />
    </Stack.Navigator>