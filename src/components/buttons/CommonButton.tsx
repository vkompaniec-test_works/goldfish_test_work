import React from 'react';
import { StyleProp, StyleSheet, TouchableOpacity, ViewStyle } from 'react-native';
import { themeColors } from '../../theme/colors';
import { AppText } from '../text/AppText';


type CommonButtonType = {
    title: string;
    onPress: () => void;
    containerStyle?: StyleProp<ViewStyle>;
    isOutlined?: boolean;
    backgroundColor?: string;
    borderColor?: string;
}

export const CommonButton = ({
    title,
    onPress,
    isOutlined,
    borderColor = themeColors.white,
    backgroundColor = themeColors.blue,
    containerStyle = {}
}: CommonButtonType) =>
    <TouchableOpacity
        onPress={onPress}
        style={StyleSheet.flatten([styles.container,
        {
            backgroundColor: isOutlined ? backgroundColor : 'transparent',
            borderColor: isOutlined ? borderColor : 'transparent'
        },
            containerStyle,

        ])}
    >
        <AppText>{title}</AppText>
    </TouchableOpacity>

const styles = StyleSheet.create({
    container: {
        minWidth: 343,
        minHeight: 44,
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 1,
        borderRadius: 12
    }
})