import React from 'react';
import { StyleProp, StyleSheet, TouchableOpacity, ViewStyle } from 'react-native';
import { AppText } from '../text/AppText';

type TextButtonProps = {
    title: string;
    onPress: () => void;
    style?: StyleProp<ViewStyle>;
}

export const TextButton = ({ title, onPress, style = {} }: TextButtonProps) =>
    <TouchableOpacity style={style} onPress={onPress}><AppText>{title}</AppText></TouchableOpacity>

export const styles = StyleSheet.create({
    text: {

    }
})