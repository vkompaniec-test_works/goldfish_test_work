import React from 'react';
import { StyleProp, StyleSheet, Text, TextStyle } from 'react-native';
import { themeColors } from '../../theme/colors';

type AppTextType = {
    children: React.ReactElement | string;
    style?: TextStyle;
}

export const AppText = ({ children, style = {} }: AppTextType) => <Text style={[styles.text, style]}>{children}</Text>;

const styles = StyleSheet.create({
    text: {
        fontFamily: 'Manrope-Regular',
        fontSize: 14,
        lineHeight: 21,
        color: themeColors.white
    }
})