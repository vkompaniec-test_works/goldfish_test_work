import React, { useCallback } from 'react';
import { StyleSheet, View } from 'react-native';
import { themeColors } from '../../../theme/colors';
import LogoImage from '../../../../assets/svgs/logo.svg';
import { AppText } from '../../../components/text/AppText';
import { TextButton } from '../../../components/buttons/TextButton';
import { SafeAreaView } from 'react-native-safe-area-context';
import { CommonButton } from '../../../components/buttons/CommonButton';
import { ScreenRoutes } from '../../../navigation/screenRoutes';
import { InitScreenProps } from '../../../navigation/authNavigator/types';

export const InitScreen = ({ navigation }: InitScreenProps) => {

    const handleSigninPress = () => { };

    const handleRegistrationPress = useCallback(() => navigation.navigate(ScreenRoutes.REGISTRATION_SCREEN), []);


    return <SafeAreaView style={styles.screen}>
        <View style={styles.container}>
            <LogoImage style={styles.logo} />
            <AppText style={styles.title}>Программа поддержки пациентов и врачей</AppText>

        </View><View style={styles.buttons}>
            <TextButton title="Регистрация" onPress={handleRegistrationPress} />
            <CommonButton title="Вход" onPress={handleSigninPress} isOutlined containerStyle={styles.signin} />
        </View>
    </SafeAreaView>
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: themeColors.blue
    },
    logo: {
        marginTop: 187,
    },
    container: {
        flex: 1,
        alignItems: 'center',
    },
    title: {
        fontSize: 30,
        lineHeight: 34,
        fontWeight: '600',
        color: themeColors.white,
        marginTop: 16,
        width: 332,
        textAlign: 'center'
    },
    buttons: {
        positiom: 'absolute',
        bottom: 126,
        alignItems: 'center'
    },
    signin: {
        marginTop: 16
    }
})