import React from 'react';
import { RootNavigator } from './src/navigation/rootNavigator/rootNavigator';

const App = (): JSX.Element => <RootNavigator />

export default App;
